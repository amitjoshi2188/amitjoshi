<?php

defined('BASEPATH') OR exit('No direct Script access allowed');

if (!function_exists('_pre')):

    function _pre($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        return $array;
    }


endif;