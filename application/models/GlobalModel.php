<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GlobalModel extends CI_Model {

    public function getAll($tableName) {
        $query = $this->db->select('*')->from($tableName)->get();
        $count = $query->num_rows();
        if ($count && $count > 0):
            $response = $query->result();
            return $response;
        endif;
    }

    public function getAllUserDetails() {
        $this->db->select('users.*,category.name as categoryName')->from('users');
        $this->db->join('category', 'users.categoryId = category.id');
        $this->db->order_by('users.id', 'desc');
        $query = $this->db->get();
        $count = $query->num_rows();
        if ($count && $count > 0):
            $response = $query->result();
            return $response;
        endif;
    }

    public function addRecord($tableName, $data) {
        $this->db->insert($tableName, $data);
        $lastInsertedId = $this->db->insert_id();
        if ($lastInsertedId):
            return $lastInsertedId;
        endif;
    }

    public function deleteRecord($tableName, $ids) {
        $this->db->where_in('id', $ids);
        $delete = $this->db->delete($tableName);
//        echo $this->db->last_query();exit;
        return $delete;
    }

    public function updateRecord($tableName, $data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update($tableName, $data);
        $row = $this->db->affected_rows();
        return $row;
    }

    public function getOnBy($tableName, $id) {
        $result = '';
        $this->db->select('*')->from($tableName)->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0):
            $result = $query->row();
        endif;
        return $result;
    }

}
