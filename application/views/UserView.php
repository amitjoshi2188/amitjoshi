
<html>
    <head>
        <title>Users Listing View</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
    <body>
        <form id="saveUser" name="saveUser" method="post"enctype="multipart/form-data">
            <table id="addUser" name="addUser" align="center" border="1">
                <tr>
                    <td>Name</td>
                    <td><input type="text" name="name"></td>
                </tr>
                <tr>
                    <td>Contact No</td>
                    <td><input type="text" name="contactNo"></td>
                </tr>
                <tr>
                    <td>Hobbies</td>
                    <td>
                        <input type="checkbox" name="hobbies[]" value="Programming">Programming
                        <input type="checkbox" name="hobbies[]" value="Games">Games
                        <input type="checkbox" name="hobbies[]" value="Photography">Photography
                        <input type="checkbox" name="hobbies[]" value="Reading">Reading
                    </td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td><select name="category" id="category">
                            <option value=""> select Category</option>
                            <?php foreach ($category as $key => $value) { ?>
                                <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>ProfilePic</td>
                    <td><input type="file" name="profilePic" id="profilePic"></td>
                </tr>
                <tr>
                    <td><input type="reset" name="reset" value="reset"></td>
                    <td><input type="submit" value="submit" name="submit"></td>
                </tr>
            </table>
        </form>

        <form name="editUser" id="editUser" method="post"enctype="multipart/form-data">
            <span style=""><a href="javascript:void(0);" onclick="showForm();" id="addNew" name="addNew">Add New</a> | <a href="javascript:void(0);" id="BulkDelete" name="BulkDelete">Bulk Delete</a> |<a href="javascript:void(0);" onclick="showListing();" id="showListing" name="showListing">showListing</a></span>
            <table border="1" align="center" class="listing">
                <thead>
                    <tr>
                        <th>SrNo</th>
                        <th>Select</th>
                        <th>Name</th>
                        <th>Contact No</th>
                        <th>Hobbies</th>
                        <th>Category</th>
                        <th>ProfilePic</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
<!--                <select id="mycategory"></select>-->
                    <?php
                    if ($getUsers):

                        foreach ($getUsers as $key => $value) {
                            ?>
                            <tr id="<?= $value->id; ?>">

                                <td class="<?= $value->id; ?>currentRow all"><?= $value->id; ?></td>
                                <td class="<?= $value->id; ?>currentRow all"><input type="checkbox" name="id[]" id="" value="<?= $value->id; ?>"></td>
                                <td class="<?= $value->id; ?>currentRow all" id="<?= $value->id; ?>nameLabel" value="<?= $value->name; ?>"><?= $value->name; ?></td>
                                <td class="<?= $value->id; ?>currentRow all" ><?= $value->contactNo; ?></td>
                                <td class="<?= $value->id; ?>currentRow all"><?= $value->hobbies; ?></td>
                                <td class="<?= $value->id; ?>currentRow all"><?= $value->categoryName; ?></td>
                                <td class="<?= $value->id; ?>currentRow all"> <img width="50px;" src="uploads/<?= $value->profilePic; ?>"></img></td>
                                <td class="<?= $value->id; ?>modes all">
                                    <a href="javascript:void(0);" onclick="Edit(<?= $value->id; ?>)">Edit</a> | <a href="javascript:void(0);" onclick="Delete(<?= $value->id; ?>)">Delete</a>

                                </td>
                            </tr>
                        <?php }else:
                        ?>
                        <tr>
                            <td colspan="8" style="text-align: center">No record Found</td>
                        </tr>
                    <?php endif;
                    ?>
                </tbody>
            </table>
        </form>
    </body>
</html>

<script>

    /*edit starts*/
    $('#editUser').submit(function (e) {
        event.preventDefault(e);
        var url = "User/AddUser/";
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            data: new FormData(this),
            processData: false,
            contentType: false,
            beforeSend: function (res) {
            },
            success: function (response) {
                if (response.status) {
                    alert(response.message);
                    location.reload(true);
                } else {
                    alert(response.error);
                }
            }
        });
    });
    /*edit ends*/


    $('#saveUser').submit(function (e) {
        event.preventDefault(e);
        var url = "User/AddUser/";
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            data: new FormData(this),
            processData: false,
            contentType: false,
            beforeSend: function (res) {
            },
            success: function (response) {
                if (response.status) {
                    alert(response.message);
                    location.reload(true);
                } else {
                    alert(response.error);
                }
            }
        });
    });

    $(function () {
        $("#addUser").hide();
        $('#BulkDelete').click(function () {
            var id = [];
            $(':checkbox:checked').each(function (i) {
                id[i] = $(this).val();
            });
            if (id.length === 0)
            {
                alert("Please Select atleast one checkbox");
            } else
            {
                if (confirm("Are you sure you want to delete Selected records ?"))
                {
                    $.ajax({
                        url: 'User/BulkDelete',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function (res) {
                        },
                        success: function (response)
                        {
                            if (response.status) {
                                alert(response.message);
                                for (var i = 0; i < id.length; i++)
                                {
                                    $('tr#' + id[i] + '').css('background-color', '#ccc');
                                    $('tr#' + id[i] + '').fadeOut('slow');
                                }
                            }
                        }
                    });
                } else
                {
                    return false;
                }
            }
        });
    });
    function Edit(id) {
        var categories = <?php echo json_encode($category); ?>;
        $('.modes all').hide();
        $.ajax({
            url: 'User/getById',
            type: 'POST',
            dataType: 'json',
            data: {id: id},
            beforeSend: function (res) {
                $('.all').show();
                $('.editClass').remove();
                $('.' + id + 'currentRow').hide();
                $('.' + id + 'modes').hide();

            },
            success: function (response)
            {
                var res = response.data.hobbies.split(",");
                $.each(res, function (key, value) {
                    console.log(value);
                });
                if (response.status) {
                    $("#" + id).append('<td class="editClass"><input type="hidden" name="id" id="id" value="' + response.data.id + '">' + response.data.id + '</td>');
                    $("#" + id).append('<td class="editClass"></td>');
                    $("#" + id).append('<td class="editClass"><input type="text" name="name" value="' + response.data.name + '"></td>');
                    $("#" + id).append('<td class="editClass"><input type="text" name="contactNo" id="contactNo" value="' + response.data.contactNo + '"></td>');
                    $("#" + id).append('<td class="editClass"><input type="checkbox" name="hobbies[]" value="Programming">Programming<input type="checkbox" name="hobbies[]" value="Games">Games<input type="checkbox" name="hobbies[]" value="Photography">Photography<input type="checkbox" name="hobbies[]" value="Reading">Reading</td>');
                    $("#" + id).append('<td class="editClass"><select name="category" id="mycategory"><option>select Category</option></select></td>');
                    $("#" + id).append('<td class="editClass"><input type="file" name="profilePic" id="profilePic" value="' + response.data.profilePic + '"><img width="50px;" src="uploads/' + response.data.profilePic + '"></img></td>');

                    $("#" + id).append('<td class="editClass"><input type="submit" name="submit" value="update"></td>');
                }
                $.each(categories, function (key, category) {
                    $('#mycategory').append($('<option>', {value: category.id, text: category.name}));
                    if (category.id == response.data.categoryId) {
                        $("#mycategory > [value=" + response.data.categoryId + "]").attr("selected", "true");
                        return;
                    }
                });
            }
        });
    }


    function Delete(id) {
        if (id !== '') {
            if (confirm("Are you sure you want to delete Selected record ?"))
            {
                $.ajax({
                    url: 'User/BulkDelete',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    beforeSend: function (res) {
                    },
                    success: function (response)
                    {
                        if (response.status) {
                            alert(response.message);
                            $('tr#' + id + '').css('background-color', '#ccc');
                            $('tr#' + id + '').fadeOut('slow');
                        }
                    }
                });
            } else {
                return false;
            }
        }
    }
    function showForm() {
        $(".listing").hide();
        $("#addUser").show();
    }

    function showListing() {
        $(".listing").show();
        $("#addUser").hide();
    }
</script>