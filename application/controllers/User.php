<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $tableName = "users";
        $tableName2 = "category";
        $data['category'] = $this->GlobalModel->getAll($tableName2);
        $data['getUsers'] = $this->GlobalModel->getAllUserDetails();
//        _pre($data['getUsers']);exit;
        $this->load->view('UserView', $data);
    }

    public function BulkDelete() {
        $tableName = "users";
        $ids = $this->input->post('id');
        $count = 0;
        if (is_array($ids)):
            foreach ($ids as $key => $value):
                $deleteRecord = $this->GlobalModel->deleteRecord($tableName, $value);
                $count++;
            endforeach;
        else:
            $deleteRecord = $this->GlobalModel->deleteRecord($tableName, $ids);
            $count++;
        endif;
        if ($count):
            $response = array('status' => TRUE, 'message' => 'Record Deleted Successfully');
        else:
            $response = array('status' => FALSE, 'error' => 'Something went wrong');
        endif;
        echo json_encode($response);
    }

    public function AddUser() {

        $this->form_validation->set_rules('id', 'User id', 'trim');
        $this->form_validation->set_rules('name', 'User Name', 'required|trim');
        $this->form_validation->set_rules('contactNo', 'User contact No', 'required|trim');
        $this->form_validation->set_rules('hobbies[]', 'User hobbies', 'required|trim');
        $this->form_validation->set_rules('category', 'User category', 'required|trim');
        if ($this->form_validation->run() == TRUE):
            $id = $this->input->post('id');
            $name = $this->input->post('name');
            $contactNo = $this->input->post('contactNo');
            $hobbies = $this->input->post('hobbies');
            $category = $this->input->post('category');
            $profile = time() . $_FILES['profilePic']['name'];
            if (is_array($hobbies)):
                $hobbiesArray = implode(",", $hobbies);
            else:
                $hobbiesArray = $hobbies;
            endif;
            /* starts */
            if ($_FILES['profilePic']['name'] !== ''):
                $config = array(
                    'file_name' => $profile,
                    'upload_path' => 'uploads/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'max_size' => '2000',
                    'max_height' => '1080',
                    'max_width' => '1920'
                );
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('profilePic')):
                    $message = array('error' => $this->upload->display_errors());
                else:
                    $image_data = $this->upload->data();
                endif;
            endif;
            if ($_FILES['profilePic']['name'] == ''):
                $data = array(
                    'name' => $name,
                    'contactNo' => $contactNo,
                    'hobbies' => $hobbiesArray,
                    'categoryId' => $category,
                );
            else:
                $data = array(
                    'name' => $name,
                    'contactNo' => $contactNo,
                    'hobbies' => $hobbiesArray,
                    'categoryId' => $category,
                    'profilePic' => $profile
                );

            endif;

            $tableName = "users";
            if ($id):
                $addRecord = $this->GlobalModel->updateRecord($tableName, $data, $id);
                if ($addRecord):
                    $response = array('status' => TRUE, 'message' => "Record Updated Successfully.");
                endif;
            else:
                $addRecord = $this->GlobalModel->addRecord($tableName, $data);
                if ($addRecord):
                    $response = array('status' => TRUE, 'message' => "Record Added Successfully.");
                endif;
            endif;
        else:
            $error = validation_errors();
            $response = array('status' => false, 'error' => $error);
        endif;
        echo json_encode($response);
    }

    public function getById() {
        $id = $this->input->post('id');
        $this->form_validation->set_rules('id', 'Record Id', 'required');
        if ($this->form_validation->run() == FALSE):
            $error = validation_errors();
            $response = array('status' => false, 'error' => $error);
        else:
            $tableName = "users";
            $record = $this->GlobalModel->getOnBy($tableName, $id);
            if ($record):
                $response = array('status' => TRUE, 'data' => $record);
            endif;
        endif;
        echo json_encode($response);
    }

}
