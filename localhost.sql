

-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `ajtest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ajtest`;

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Stores Category Ids',
  `name` varchar(255) NOT NULL COMMENT 'Stores Category Name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `category` (`id`, `name`) VALUES
(1,	'Developer'),
(2,	'Designer'),
(3,	'QA'),
(4,	'BA');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'stores Usres Ids',
  `name` varchar(255) NOT NULL COMMENT 'stores User name',
  `contactNo` varchar(50) NOT NULL COMMENT 'stores Users Contact no',
  `hobbies` varchar(255) NOT NULL COMMENT 'Stores Users Hobbies',
  `categoryId` int(11) NOT NULL COMMENT 'Stores users Category id',
  `profilePic` varchar(255) NOT NULL COMMENT 'Stores Users Profile Pics',
  PRIMARY KEY (`id`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `name`, `contactNo`, `hobbies`, `categoryId`, `profilePic`) VALUES
(22,	'pinal',	'9173000397',	'Games,Photography',	2,	'1557338810Sample.jpg'),
(23,	'pinal',	'9173000397',	'Games,Photography',	2,	'1557338832Sample.jpg'),
(36,	'martha wilson',	'8320084017',	'Programming,Photography',	4,	'1557340507Sample.jpg'),
(37,	'Dean kokram',	'9876543210',	'Photography,Reading',	2,	'1557340993Sample.jpg'),
(38,	'sten',	'7412589632',	'Programming,Games',	1,	'1557341179Sample.jpg');

-- 2019-05-08 18:57:07
